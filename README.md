# Timber Clone

This is a simple SFML android app clone of many popular android apps. This 
program was made with the help of a tutorial. Graphics from the program are 
currently from the tutorial. Both the code and the graphics are to be 
personalized.

## Contact

mr.carpinelli@protonmail.ch

## Contributing

This is mostly a personal project, but any contributions are welcome and
appreciated.

## Building

SFML is required.

## License

All contributions are made under the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html). See the [LICENSE](LICENSE).
